FROM python:slim-buster

LABEL DistBase="Debian 10 - Buster"

RUN apt-get update -y && \
	apt-get install -y shellcheck --no-install-recommends && \
	apt-get autoremove -y && apt-get clean -y && \
	rm -rf /var/lib/apt/lists/* && \
	pip install yamllint pylint

# Syntax-check software pre-configurations
COPY scripts/docker-entrypoint.sh /var/tmp/

RUN chmod +x /var/tmp/docker-entrypoint.sh

ENTRYPOINT ["/var/tmp/docker-entrypoint.sh"]

CMD ["code-linter"]
#docker build -t oscarenzo/code-linter:latest .
#docker buildx build --push -t oscarenzo/code-linter:latest --platform linux/amd64,linux/arm/v7 .
