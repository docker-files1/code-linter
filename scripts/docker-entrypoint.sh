#!/bin/bash
# shellcheck disable=SC2086

set -e

if [ "$1" = "code-linter" ]; then
	MAINPATH="/mnt" # Set the default point mount to the projects

	if [ -z "$CHECKPATH" ]; then
		export CHECKPATH="/"
	fi

	if [ -z "$CHECKTYPE" ]; then
		export CHECKTYPE="none"
	else
		if [ -z "$ARGS" ]; then
			export ARGS=""
		fi
	fi

	# Run the checks in order to type
	case "$CHECKTYPE" in
		"yamllint")
			for path in $(echo ${CHECKPATH} | tr "," " "); do
				# If the path string do not finish with /
				if [ "${path: -1}" != "/" ]; then path="${path}/"; fi

				# If the path string do not start with /
				if [ "${path: 0:1}" != "/" ]; then path="/${path}"; fi

				cd "${MAINPATH}$path" || exit 2

				yamllint ${ARGS} -- *.yml
			done
		;;
		"shellcheck")
			for path in $(echo ${CHECKPATH} | tr "," " "); do
				# If the path string do not finish with /
				if [ "${path: -1}" != "/" ]; then path="${path}/"; fi

				# If the path string do not start with /
				if [ "${path: 0:1}" != "/" ]; then path="/${path}"; fi

				cd "${MAINPATH}$path" || exit 2

				shellcheck -s bash ${ARGS} -- *.sh
			done
		;;
		"pylint")
			for path in $(echo ${CHECKPATH} | tr "," " "); do
				# If the path string do not finish with /
				if [ "${path: -1}" != "/" ]; then path="${path}/"; fi

				# If the path string do not start with /
				if [ "${path: 0:1}" != "/" ]; then path="/${path}"; fi

				cd "${MAINPATH}$path" || exit 2

				PYRESULT="$(pylint ${ARGS} -- *.py ; true)"
				SCORE="$(echo "$PYRESULT" | grep -oE "1?[0-9]\.[0-9]" | head -n 1 | awk '{print $1}' FS='.')"

				echo "$PYRESULT"

				if [ "$SCORE" -le 8 ]; then exit 2; else exit 0; fi
			done
		;;
		"none")
			/bin/bash
		;;
	esac

cat << EOB

******************************************************
*                                                    *
*    Docker image: oscarenzo/code-linter             *
*    https://gitlab.com/docker-files1/code-linter    *
*                                                    *
******************************************************
SERVER SETTINGS
---------------
· Python version: 3.10
· Ansible version: 2.7.7
· Components: pylint, yamllint, shellcheck
---------------

EOB
else
	set -- "$@"
	exec "$@"
fi
