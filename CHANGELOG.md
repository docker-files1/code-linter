# [0.11.0](/Docker_Files/code-linter/compare/0.11.0...main)
* CICD migraton from Jenkins to GitlabCI

# [0.10.0](/Docker_Files/code-linter/compare/0.10.0...main)
* Docker linter apply and little hotfix that might affect to some files that match with the current `.gitignore`

# [0.9.0](/Docker_Files/code-linter/compare/0.9.0...main)
* Se actualiza el formato de la documentación

# [0.8.1](/Docker_Files/code-linter/compare/0.8.1...main)
* Corección al momento de detectar el score en `pylint`, detectaba **0** cuando el *score* era de **10**

# [0.8.0](/Docker_Files/code-linter/compare/0.8.0...main)
* Se habilita el chequeo de scripts pyhton usando `pylint`, con soporte a *pyhton >= 3*
    * Dado que al instalar el binario de `pylint` instala el que da soporte a python 2, se procede a hacer la instalación a través de PIP
    * Con ello se instala de la misma manera `yamllint`

# [0.7.0](/Docker_Files/code-linter/compare/0.7.0...main)
* Se actualiza el jenkinsfile segun la nueva versión de la *Shared Library*
* Se retira soporte para armv7

# [0.6.1](/Docker_Files/code-linter/compare/0.6.1...main)
* Se normaliza `PATH` por defecto del contenedor a **/mnt** y se actualiza documentación que hace alusión a ello.

# [0.6.0](/Docker_Files/code-linter/compare/0.6.0...main)
* Se reemplaza la nomenclatura en el compilador virtual para la arquitectura `linux/arm64/v8`, ahora se denomina `linux/arm64` por **buildx**, reemplazamos valor en variable `platform` del *Jenkinsfile*

# [0.5.1](/Docker_Files/code-linter/compare/0.5.1...main)
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados

# [0.5.0](/Docker_Files/code-linter/compare/0.5.0...main)
* Se ajusta el `Jenkinsfile` con nuevas reglas de validación

# [0.4.0](/Docker_Files/code-linter/compare/0.4.0...main)
* Se eliminan del `Dockerfile`, las etiquetas genéricas ya que son insertadas al momento de hacer el build de manera automática.
* Se actualiza la configuración por defecto del *.editorconfig* y el *.gitignore*
* Se ajusta el `Jenkinsfile` para informar sobre el path donde se encuentra las `Shared libs`
* Se ajusta el `README` para que muestre información relevante

# [0.3.0](/Docker_Files/code-linter/compare/0.3.0...main)
* Inicialización del `CHANGELOG`
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `dockerfilePipeline`
    * Se eliminan del Dockerfile, las etiquetas genéricas ya que son insertadas al momento de hacer el build de manera automática.
* Lintado del ficheros `Vagrantfile`
