[![pipeline status](https://gitlab.com/docker-files1/code-linter/badges/development/pipeline.svg)](https://gitlab.com/docker-files1/code-linter/-/commits/development) ![Project version](https://img.shields.io/docker/v/oscarenzo/code-linter?sort=date) ![Docker image pulls](https://img.shields.io/docker/pulls/oscarenzo/code-linter) ![Docker image size](https://img.shields.io/docker/image-size/oscarenzo/code-linter?sort=date) ![Project license](https://img.shields.io/gitlab/license/docker-files1/code-linter)

# Code-linter
This project work with some components like yamllint, pylint in order to check your code linter.

## 🧾 Components
 - Debian
 - Ansible
 - Python3
 - Pylint
 - Yamllint
 - Shellcheck

## 🔗 References
https://github.com/docker-library/python/blob/56cea612ab370f3d05b29e97466d418a0f07e463/3.10/slim-buster/Dockerfile

## ✍️ Advices
By default the program will look for the **Git project repository** on this location.

```
/mnt/
```

So if you ask to check the path `/foo/var`, really you are asking inside the container `/mnt/foo/bar`, this is transparent for the user.

### Usage
For run checks need to run in one line:

```
docker run -it --rm -e CHECKPATH=/dir/to/check[,/dir/to/check/another] -e CHECKTYPE=yamllint -v /mycode/dir:/mnt oscarenzo/code-linter:latest

docker run -it --rm -e CHECKPATH=/dir/to/check[,/dir/to/check/another] -e CHECKTYPE=shellcheck -e ARGS="-e SC2086" -v /mycode/dir:/mnt oscarenzo/code-linter:latest

docker run -it --rm -e CHECKPATH=/dir/to/check[,/dir/to/check/another] -e CHECKTYPE=pylint -e ARGS="--reports y" -v /mycode/dir:/mnt oscarenzo/code-linter:latest
```

## ⚙️ Environment variables
This image uses environment variables to allow the configuration of some parameters at run time:

* Variable name: `CHECKPATH`
* Default value: /mnt
* Accepted values: A string format.
* Description: The location in the container where is the code directory to start to lint check, you can pass multiple location in commas separated (no spaces), by example */foo/dir1/,/foo/dir2/*, If you don't specify it through the `CHECKPATH` environment variable at run time, `/mnt` will be used by default.

----

* Variable name: `CHECKTYPE`
* Default value: none
* Accepted values: A string format.
* Description: The select the check type that want to use, it actually supported *yamllint*, *pylint* and *shellcheck*. If you don't specify it through the `CHECKTYPE` environment variable at run time, `none` will be used by default.

----

* Variable name: `ARGS`
* Default value: NULL
* Accepted values: A string format.
* Description: When run the commands shellcheck, yamllint and ansible-lint, you can pass some arguments, you can use this variable for it. If you don't specify it through the `ARGS` environment variable at run time, `NULL` will be used by default.

## 💬 Legend
* NKS => No key sensitive
* KS => Key sensitive
